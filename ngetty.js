#!/usr/bin/env node

console.log('Starting nodeos-getty');

var cp = require('child_process');
var fs = require('fs');
var io = require('./io');

// Defaults
// On Ubuntu /dev/console and /dev/tty0 are aliases for the 
// current TTY. The *actual* console is /dev/tty1
//
// Should migrate these to --tty=XXX and --shell=XXX
var ttypath = '/dev/tty1';
var shell   = '/bin/bash';

// Must be session leader to set a controlling TTY
// Sometimes you're already the sesion leader,
// so we'll leave any following alone.
if( io.setsid() < 0 )
  console.log('Warn: Cannot become session leader');

// Open a TTY as the session leader *should* make it
// the controlling TTY
var tty = fs.openSync(ttypath,'r+');

// If the above doesn't work, we explicitly set
// the controlling TTY. If this fails, something
// went wrong and we should bail.
if( io.setControllingTTY(tty) != 0 )
  return console.log('Cannot assume controlling TTY');

// Need to duplicate the fds before we pass them
// off to the child process
var ttyi = tty;
var ttyo = io.dup(tty);
var ttye = io.dup(tty);

// Write a nice header that clears the screen first
fs.writeSync(ttyo,'\033[2J\033[1;1H\n');
fs.writeSync(ttyo,'Welcome to NodeOS\n');
fs.writeSync(ttyo,'-----------------\n\n');

// Normal Node.js child process magic
var comm = shell;
var args = [];
var opts = {
  cwd: process.cwd,
  env: process.env,
  stdio: [ttyi,ttyo,ttye]
};

// Cleanup
fs.close(ttyi);
fs.close(ttyo);
fs.close(ttye);

// Handle Exits
var prog = cp.spawn(comm,args,opts);
prog.on('error',function(err){
  console.log('Error Spawning Shell %s: %s',shell,err);
});
prog.on('exit',function(code){
  console.log('Shell %s Exited with Code %d',shell,code);
  process.exit(code);
});

console.log('ngetty listening to',ttypath);
